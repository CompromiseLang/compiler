<div align="center">
    <h1>
        <a href="https://gitlab.com/CompromiseLang/compiler">
          <img src="docs/images/logo.png" alt="Logo" width="100" height="100">
        </a>
        <br />
        Compromise
    </h1>
</div>

<div align="center">
    <i>General-purpose programming language designed for ease of readability, reasoning, extensibility, and robust simplicity</i>
    <br />
    <a href="https://gitlab.com/CompromiseLang/compiler/issues/new">
        Report a Bug
    </a>
    ·
    <a href="https://gitlab.com/CompromiseLang/compiler/issues/new">
        Request a Feature
    </a>
    .
    <a href="https://gitlab.com/CompromiseLang/compiler/issues/new">
        Ask a Question
    </a>
<br />

[![Project license](https://img.shields.io/badge/license-MIT-green)](LICENSE)

[![Pull Requests welcome](https://img.shields.io/badge/PRs-welcome-ff69b4.svg?style=flat-square)](https://gitlab.com/CompromiseLang/compiler/compare)
[![code with love by](https://img.shields.io/badge/%3C%2F%3E%20with%20%E2%99%A5%20by-Compromise-ff1414.svg?style=flat-square)](https://gitlab.com/CompromiseLang)

Donations accepted:
<br />
[![Liberapay receiving](https://img.shields.io/liberapay/receives/cvoges12.svg?style=flat-square)](https://liberapay.com/cvoges12)
[![buymeacoffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://buymeacoffee.com/cvoges12)
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/cvoges12)

</div>


## Table of Contents

- [About](#about)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Usage](#usage)
- [Support](#support)
- [Roadmap](#roadmap)
- [Project Help](#project-help)
- [Contributing](#contributing)
- [Acknowledgements](#acknowledgements)
    - [Funding Organizations](#funding-organizations)
    - [Patrons](#patrons)
    - [Software Used](#software-used)
    - [Inspirations](#inspirations)
    - [Authors and Contributors](#authors-and-contributors)
- [Security](#security)
- [License](#license)


## About

**[Back to top](#table-of-contents)**

## Getting started

### Prerequisites

Required dependencies:
- git
- cmake >= v3.0.0
- clang >= v12.0.0
- LLVM, Clang, LLD development libraries == 14.x compiled with the same clang version above

**[Back to top](#table-of-contents)**

### Installation

After installing the dependencies, 
run the following to build:
```sh
git clone https://gitlab.com/CompromiseLang/compiler.git
cd compiler
# add more build instructions
```

**[Back to top](#table-of-contents)**

## Usage

See the [language documentation and specification]() 
along with [compiler document]() for usage.

If compiler does not work, use the parser and syntactic checker for accuracy.

**[Back to top](#table-of-contents)**

## Support

**[Back to top](#table-of-contents)**

## Roadmap

See the [open issues](https://gitlab.com/CompromiseLang/compiler/issues) 
for a list of proposed features (and known issues).

**[Back to top](#table-of-contents)**

## Project Help

If you want to say "thank you" and/or support active development of 
**grdl**:
- Add a [GitHub Star](https://github.com/CompromiseLang/compiler) to the 
project's GitHub awareness page. (*our project is not mirrored to prevent 
GitHub Copilot*)
- Tweet about grdl or spread the word on other social media.
- Write interesting articles about the project on [Dev.to](https://dev.to/), 
[Medium](https://medium.com/), or your personal blog.
- Become a patron and help fund **grdl**:
  - [Liberapay](https://liberapay.com/cvoges12)
  - [Buy me a coffee](https://www.buymeacoffee.com/cvoges12)
  - [Ko-Fi](https://ko-fi.com/cvoges12)

## Contributing

Firstly, thanks for considering to contribute! 
Contributions are what make the open-source community such an amazing place to 
learn, inspire, and create. 
Any contributions you make will benefit everybody else and are **greatly 
appreciated**.

Please read [our contribution guidelines](docs/CONTRIBUTING.md), and thank you 
for being involved!

**[Back to top](#table-of-contents)**

## Acknowledgements

### Funding Organizations

**[Back to top](#table-of-contents)**

### Patrons

A special thanks to my patrons on:
- [Liberapay](https://liberapay.com/cvoges12)
- [Buy me a coffee](https://www.buymeacoffee.com/cvoges12)
- [Ko-Fi](https://ko-fi.com/cvoges12)

**[Back to top](#table-of-contents)**


### Software Used

- The 
[Amazing Github Template](https://github.com/dec0dOS/amazing-github-template) 
for the README.md and some other documents.
- The [cvoges12 app-template](https://gitdab.com/cvoges12/app-template) for 
this template repository.

**[Back to top](#table-of-contents)**

### Inspirations

**[Back to top](#table-of-contents)**

### Authors and contributors

The original setup of this repository is by 
[cvoges12](https://gitlab.com/cvoges12).

For a full list of all authors and contributors, see
[the members](https://gitlab.com/groups/CompromiseLang/-/group_members) and 
[contributors page](https://gitlab.com/CompromiseLang/compiler/-/graphs/main).

**[Back to top](#table-of-contents)**


## Security

**Compromise** follows good practices of security, but 100% security cannot 
be assured.
**Compromise** is provided **"as is"** without any **warranty**. Use at your 
own risk.

_For more information and to report security issues, please refer to our 
[security documentation](docs/SECURITY.md)._

**[Back to top](#table-of-contents)**

## License

Copyright (C) 2022 Clayton Voges

The ultimate goal of the Compromise language project is to make a simple language to 
communicate ideas elegantly in a readable format. We intend our language to be 
machine compilable in an efficient way, but the way that a human reasons about 
the language is first and for most.

However, the language is made in hope to improve the quality of as much 
software as possible and to ease the burden of software developers. Because of 
this, the language aims to be widespread. So we distribute this under the MIT 
License with a humble request: make harmless software that serves the end 
users.



This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the [MIT License](LICENSE) for more details.

You should have received a copy of the [MIT License](LICENSE) along with 
this program. If not, see <https://opensource.org/licenses/MIT>.
